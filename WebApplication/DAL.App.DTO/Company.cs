﻿using System;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Company : DomainEntityId
    {
        public Guid CompanyId { get; set; }
        
        public string Name { get; set; }
        
    }
}