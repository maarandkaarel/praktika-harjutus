﻿using System;
using System.Collections.Generic;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Leg : DomainEntityId
    {
        public Guid LegId { get; set; }
        
        public RouteInfo RouteInfo { get; set; }
        
        public List<Provider> Providers { get; set; }
    }
}