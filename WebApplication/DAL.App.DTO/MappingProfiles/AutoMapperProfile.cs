﻿using AutoMapper;

namespace DAL.App.DTO.MappingProfiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<DAL.App.DTO.Company, Domain.App.Company>().ReverseMap();
            CreateMap<DAL.App.DTO.Leg, Domain.App.Leg>().ReverseMap();
            CreateMap<DAL.App.DTO.Planet, Domain.App.Planet>().ReverseMap();
            CreateMap<DAL.App.DTO.Provider, Domain.App.Provider>().ReverseMap();
            CreateMap<DAL.App.DTO.Reservation, Domain.App.Reservation>().ReverseMap();
            CreateMap<DAL.App.DTO.Root, Domain.App.Root>().ReverseMap();
            CreateMap<DAL.App.DTO.RouteInfo, Domain.App.RouteInfo>().ReverseMap();
        }
    }
}