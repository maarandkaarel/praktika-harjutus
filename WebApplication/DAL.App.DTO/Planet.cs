﻿using System;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Planet : DomainEntityId
    {
        public Guid PlanetId { get; set; }
        
        public string Name { get; set; }
    }
}