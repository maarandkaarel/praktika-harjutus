﻿using System;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Provider : DomainEntityId
    {
        private Guid ProviderId { get; set; }
        
        public Company Company { get; set; }
        
        public double Price { get; set; }
        
        public DateTime FlightStart { get; set; }
        
        public DateTime FlightEnd { get; set; }
    }
}