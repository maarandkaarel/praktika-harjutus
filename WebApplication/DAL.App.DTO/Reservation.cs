﻿using System;
using System.Collections.Generic;
using Contracts.Domain.Base;
using Domain.Base;
using Domain.Identity;

namespace DAL.App.DTO
{
    public class Reservation : DomainEntityId, IDomainAppUserId, IDomainAppUser<AppUser>
    {
        public Guid ReservationId { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public List<RouteInfo> Routes { get; set; }
        
        public double TotalQuotedPrice { get; set; }
        
        public TimeSpan QuotedTravelTime { get; set; }
        
        public List<string> TransportationCompanyNames { get; set; }
        public Guid AppUserId { get; set; }
        
        public AppUser? AppUser { get; set; }
    }
}