﻿using System;
using System.Collections.Generic;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Root : DomainEntityId
    {
        public Guid RootId { get; set; }
        
        public DateTime ValidUntil { get; set; }
        
        public List<Leg> Legs { get; set; }
    }
}