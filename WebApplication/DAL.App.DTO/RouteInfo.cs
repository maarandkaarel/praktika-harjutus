﻿using System;
using Domain.Base;

namespace DAL.App.DTO
{
    public class RouteInfo : DomainEntityId
    {
        public Guid RouteInfoId { get; set; }
        
        public Planet From { get; set; }
        
        public Planet To { get; set; }
        
        public int Distance { get; set; }
    }
}