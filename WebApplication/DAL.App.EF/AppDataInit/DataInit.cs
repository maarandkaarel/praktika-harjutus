﻿using System;
using DAL.AppDataInit.Enums;
using Domain.App.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.AppDataInit
{
    public class DataInit
    {
        public static void SeedAppData(ApplicationDbContext context)
        {
        }

        public static void DropDatabase(ApplicationDbContext context)
        {
            context.Database.EnsureDeleted();
        }
        public static void MigrateDatabase(ApplicationDbContext context)
        {
            context.Database.Migrate();
        }

        public static void DeleteDatabase(ApplicationDbContext context)
        {
            context.Database.EnsureDeleted();
        }
        public static void SeedRoles(RoleManager<AppRole> roleManager)
        {
            foreach (var roleName in Enum.GetValues(typeof(Roles)))
            {
                var role = new AppRole();
                role.Name = roleName.ToString();
                roleManager.CreateAsync(role).Wait();
            }
        }

        public static void SeedUsers(UserManager<AppUser> userManager)
        {
            string masterEmail = "maarandkaarel@gmail.com";

            var master = new AppUser()
            {
                UserName = masterEmail,
                Email = masterEmail,
                FirstName = "Kaarel-Martin",
                LastName = "Maarand",
                PhoneNumber = "59057050",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };

            var user = userManager.FindByEmailAsync(masterEmail).Result;
            
            if (user == null)
            {
                var createMainUser = userManager.CreateAsync(master, "Admin123.").Result;
                if (createMainUser.Succeeded)
                {
                    foreach (var roleName in Enum.GetValues(typeof(Roles)))
                    {
                        userManager.AddToRoleAsync(master, roleName.ToString()).Wait();
                    }
                }
            }
        }
    }
}