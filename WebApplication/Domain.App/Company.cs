﻿using System;
using Domain.Base;

namespace Domain.App
{
    public class Company : DomainEntityId
    {
        public Guid CompanyId { get; set; }
        
        public string Name { get; set; } = default!;
        
    }
}