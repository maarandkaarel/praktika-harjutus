﻿using System;
using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class Leg : DomainEntityId
    {
        public Guid LegId { get; set; }
        
        public RouteInfo RouteInfo { get; set; } = default!;
        
        public List<Provider> Providers { get; set; } = default!;
    }
}