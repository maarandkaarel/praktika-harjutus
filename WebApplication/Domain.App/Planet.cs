﻿using System;
using Domain.Base;

namespace Domain.App
{
    public class Planet : DomainEntityId
    {
        public Guid PlanetId { get; set; }
        
        public string Name { get; set; } = default!;
    }
}