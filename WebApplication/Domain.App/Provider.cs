﻿using System;
using Domain.Base;

namespace Domain.App
{
    public class Provider : DomainEntityId
    {
        private Guid ProviderId { get; set; }
        
        public Company Company { get; set; } = default!;
        
        public double Price { get; set; } = default!;
        
        public DateTime FlightStart { get; set; } = default!;
        
        public DateTime FlightEnd { get; set; } = default!;
    }
}