﻿#nullable enable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Text.Json.Serialization;
using Contracts.Domain.Base;
using Domain.Base;
using Domain.App.Identity;

namespace Domain.App
{
    public class Reservation : DomainEntityId/*, IDomainAppUser<AppUser>*/
    {
        public string FirstName { get; set; } = default!;
        
        public string LastName { get; set; } = default!;
        
        public string Routes { get; set; } = default!;
        public double TotalQuotedPrice { get; set; }
        public long QuotedTravelTime { get; set; } = default!;
        public string TransportationCompanyNames { get; set; } = default!;

        public DateTime ValidUntil { get; set; } = default!;

        /*public Guid AppUserId { get; set; }
        
        [JsonIgnore]
        public AppUser? AppUser { get; set; }*/
    }
}