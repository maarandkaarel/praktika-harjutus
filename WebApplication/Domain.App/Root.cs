﻿using System;
using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class Root : DomainEntityId
    {
        public Guid RootId { get; set; }
        
        public DateTime ValidUntil { get; set; } = default!;

        public List<Leg> Legs { get; set; } = default!;
    }
}