﻿using System;
using Contracts.Domain.Base;

namespace Domain.App
{
    public class RouteInfo : IDomainEntityId
    {
        public Guid RouteInfoId { get; set; }
        
        public Planet From { get; set; } = default!;
        
        public Planet To { get; set; } = default!;
        
        public int Distance { get; set; } = default!;
    }
}