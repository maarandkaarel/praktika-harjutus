﻿using System;

namespace PublicApi.DTO.v1
{
    public class Company
    {
        public Guid CompanyId { get; set; }

        public string Name { get; set; } = default!;

    }
}