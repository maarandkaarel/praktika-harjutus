﻿using System;
using System.Collections.Generic;

namespace PublicApi.DTO.v1
{
    public class Leg
    {
        public Guid LegId { get; set; }
        
        public RouteInfo RouteInfo { get; set; } = default!;
        
        public List<Provider> Providers { get; set; } = default!;
    }
}