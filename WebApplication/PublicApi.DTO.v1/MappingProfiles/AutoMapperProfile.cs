﻿using AutoMapper;

namespace PublicApi.DTO.v1.MappingProfiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Domain.App.Reservation, ReservationAdd>().ReverseMap();
            CreateMap<Domain.App.Reservation, Reservation>().ReverseMap();
        }
        
    }
}