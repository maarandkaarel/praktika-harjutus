﻿using System;

namespace PublicApi.DTO.v1
{
    public class Planet 
    {
        public Guid PlanetId { get; set; }
        
        public string Name { get; set; } = default!;
    }
}