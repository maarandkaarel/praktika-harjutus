﻿using System;

namespace PublicApi.DTO.v1
{
    public class Provider
    {
        private Guid ProviderId { get; set; }
        
        public Company Company { get; set; } = default!;
        
        public double Price { get; set; } = default!;
        
        public DateTime FlightStart { get; set; } = default!;
        
        public DateTime FlightEnd { get; set; } = default!;
    }
}