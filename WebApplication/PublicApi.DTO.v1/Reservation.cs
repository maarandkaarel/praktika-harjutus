﻿#nullable enable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Text.Json.Serialization;
using Contracts.Domain.Base;
using Domain.App.Identity;

namespace PublicApi.DTO.v1
{
    public class ReservationAdd
    {
        public string FirstName { get; set; } = default!;
        
        public string LastName { get; set; } = default!;
        
        public string Routes { get; set; } = default!;
        
        public double TotalQuotedPrice { get; set; } = default!;
        
        public long QuotedTravelTime { get; set; } = default!;
        
        public string TransportationCompanyNames { get; set; } = default!;
        
        public DateTime ValidUntil { get; set; } = default!;

        /*public Guid AppUserId { get; set; }
        
        [JsonIgnore]
        public AppUser? AppUser { get; set; }*/
        
    }
    
    public class Reservation
    {
        public Guid Id { get; set; }
        
        public string FirstName { get; set; } = default!;
        
        public string LastName { get; set; } = default!;
        
        public string Routes { get; set; } = default!;
        
        public double TotalQuotedPrice { get; set; } = default!;
        
        public long QuotedTravelTime { get; set; } = default!;
        
        public string TransportationCompanyNames { get; set; } = default!;
        
        public DateTime ValidUntil { get; set; } = default!;

        /*public Guid AppUserId { get; set; }
        
        [JsonIgnore]
        public AppUser? AppUser { get; set; }*/
        
    }
}