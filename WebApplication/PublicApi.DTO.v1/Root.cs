﻿using System;
using System.Collections.Generic;

namespace PublicApi.DTO.v1
{
    public class Root
    {
        public Guid Id { get; set; }
        
        public DateTime ValidUntil { get; set; } = default!;
        
        public List<Leg> Legs { get; set; } = default!;
    }
}