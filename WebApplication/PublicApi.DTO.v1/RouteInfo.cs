﻿using System;

namespace PublicApi.DTO.v1
{
    public class RouteInfo
    {
        public Guid Id { get; set; }

        public Planet From { get; set; } = default!;
        
        public Planet To { get; set; } = default!;
        
        public int Distance { get; set; } = default!;
    }
}