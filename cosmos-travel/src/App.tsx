import React, { useState } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import jwt_decode, { JwtPayload } from "jwt-decode";
import { IJwtLoad } from './types/IJwtLoad';

import './App.css';
import Header from './components/Header';
import RootIndex from './containers/root/RootIndex';
import { AppContextProvider, initialAppState } from './context/AppState';
import Login from './identity/login';
import Register from './identity/register';
import ReservationIndex from './containers/reservations/ReservationIndex';
import ProfileSettings from './containers/profile/ProfileSettings';

const App = () => {

  const setAuthInfo = (token: string | null, firstName: string, lastName: string, roles: string[]): void => {
    setAppState({ ...appState, token, firstName, lastName, roles });
  };

  const [appState, setAppState] = useState({ ...initialAppState, setAuthInfo });

  if (appState.token !== null) {
    var jwt = jwt_decode(appState.token) as JwtPayload extends IJwtLoad ? IJwtLoad : IJwtLoad;
    const roles = jwt["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
    console.log("roles: " + roles);
  }
  return (
    <>
      <AppContextProvider value={appState}>
      <Header />
      <div className="container">
        <main role="main" className="pb-3">
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/profile" element={<ProfileSettings />} />

            <Route path="/root" element={<RootIndex />} />
            <Route path="/reservations" element={<ReservationIndex />} />
            <Route path="/" element={<Navigate replace to="/root" />} />
          </Routes>
        </main>
      </div>
      </AppContextProvider>
    </>
  );
}

export default App;
