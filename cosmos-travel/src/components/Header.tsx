import React from 'react';
import { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { AppContext } from '../context/AppState';

const Header = () => {
    const appState = useContext(AppContext);
    console.log(appState.roles)

    return (
        <header>
            <nav className="navbar navbar-expand-sm navbar-toggleable-sm navbar-dark bg-dark border-bottom box-shadow mb-3">
                <div className="container">
                    <div className="navbar-collapse collapse d-sm-inline-flex justify-content-between">
                        <ul className="navbar-nav flex-grow-1">
                            <li>
                                <NavLink className="btn btn-link nav-link text-light" to="/root">ROOT</NavLink>
                            </li>
                            <li>
                                <NavLink className="btn btn-link nav-link text-light" to="/reservations">RESERVATIONS</NavLink>
                            </li>
                        </ul>
                        <ul className="navbar-nav">
                            {appState.token === null ?
                                <>
                                    <li className=" nav-item">
                                        <NavLink className="btn btn-link nav-link text-light" to="/login">Login {appState.firstName}</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="btn btn-link nav-link text-light" to="/register">Register</NavLink>
                                    </li>
                                </>
                                : <>
                                    <li className="nav-item">
                                        {/* <p className="text-light">{appState.firstName + ' ' + appState.lastName}</p> */}
                                        <NavLink className="btn btn-link nav-link text-light" to="/profile">{appState.firstName + ' ' + appState.lastName}</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <button onClick={() => appState.setAuthInfo(null, '', '', [])} className="btn btn-link nav-link text-light">Logout</button>
                                    </li>
                                </>
                            }
                        </ul>
                    </div >
                </div >
            </nav >
        </header >
    );
}

export default Header;
