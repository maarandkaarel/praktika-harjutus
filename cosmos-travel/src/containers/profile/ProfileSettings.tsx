import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import Loader from "../../components/Loader";
import { AppContext } from "../../context/AppState";
import { EPageStatus } from "../../types/EPageStatus";


const ProfileSettings = () => {
    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    let navigate = useNavigate();

    const appState = useContext(AppContext);

    const loadData = async () => {
        if (appState.token) {
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        }
        else {
            navigate("/", { replace: true });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <h1>Profile settings to be implemented...</h1>
            <Loader {...pageStatus} />

        </>
    );
}

export default ProfileSettings;