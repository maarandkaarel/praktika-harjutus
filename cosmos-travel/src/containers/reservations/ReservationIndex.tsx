import { useContext, useEffect, useState } from "react";
import Loader from "../../components/Loader";
import { AppContext } from "../../context/AppState";
import { IReservation } from "../../domain/IReservation";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";



const ReservationIndex = () => {
    const [Reservation, setReservation] = useState([] as IReservation[]);
    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });

    const appState = useContext(AppContext);

    const RowDisplay = (props: { Reservation: IReservation }) => {

        const appState = useContext(AppContext);
        const id = props.Reservation.id;

        async function DeleteClicked() {
            let response = await BaseService.delete('Reservations/', id, appState.token ?? '');
            if (response.ok) {
                loadData();
            }
        }

        return (
            <>
                <tr>
                    <td>
                        {props.Reservation.firstName}
                    </td>
                    <td>
                        {props.Reservation.lastName}
                    </td>
                    <td>
                        {props.Reservation.routes}
                    </td>
                    <td>
                        {props.Reservation.quotedTravelTime}
                    </td>
                    <td>
                        {props.Reservation.totalQuotedPrice}
                    </td>
                    <td>
                        {props.Reservation.transportationCompanyNames}
                    </td>
                    {/* <td>
                        {props.Reservation.validUntil}
                    </td> */}
                    <td>
                        <button className="btn btn-danger" onClick={() => DeleteClicked()}>DELETE</button>
                    </td>

                </tr>
            </>
        );
    };

    const loadData = async () => {
        let result = await BaseService.getAll<IReservation>('/reservations', appState.token ?? '');

        if (result.ok && result.data) {
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
            setReservation(result.data);
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: result.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);


    return (
        <>
            <h1>Reservations</h1>
            <table className="table">
                <thead>
                    <tr>
                        <th>
                            First Name
                        </th>
                        <th>
                            Last Name
                        </th>
                        <th>
                            Routes
                        </th>
                        <th>
                            Quoted Travel Time
                        </th>
                        <th>
                            Total Quoted Price
                        </th>
                        <th>
                            Transportation Company Names
                        </th>
                    </tr>
                    {/* <th>
                        Valid Until
                    </th> */}
                </thead>
                <tbody>
                    {Reservation.map(res =>
                        <RowDisplay Reservation={res} key={res.id} />)
                    }
                </tbody>
            </table>
            <Loader {...pageStatus} />
        </>
    );
}

export default ReservationIndex;