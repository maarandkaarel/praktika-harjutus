import { ChangeEvent, useContext, useEffect, useState } from "react";
import { ICompany } from "../../domain/ICompany";
import { ILeg } from "../../domain/ILeg";
import { IRoot } from "../../domain/IRoot";
import { BaseService } from "../../services/base-service";
import { CosmosService } from "../../services/cosmos-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IReservation, IReservationAdd } from "../../domain/IReservation";
import { AppContext } from "../../context/AppState";
import { useNavigate } from "react-router";
import Loader from "../../components/Loader";
import { IPathItem } from "../../domain/IPathItem";
import { IRouteProvider } from "../../domain/IRouteProvider";
import RangeSlider from "react-bootstrap-range-slider";

function msToTime(ms: number) {
    ms = Math.abs(ms);
    const days = Math.floor(ms / (24 * 60 * 60 * 1000));
    const daysMs = ms % (24 * 60 * 60 * 1000);
    const hours = Math.floor(daysMs / (60 * 60 * 1000));
    const hoursMs = ms % (60 * 60 * 1000);
    const minutes = Math.floor(hoursMs / (60 * 1000));
    return days + " d, " + hours + " h, " + minutes + " m";
}

const RootIndex = () => {
    const appState = useContext(AppContext);
    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });

    let navigate = useNavigate();

    const [root, setRoot] = useState({} as IRoot);
    const [roots, setRoots] = useState([] as IRoot[]);
    const [legs, setLegs] = useState([] as ILeg[]);
    const [fromPlanet, setFromPlanet] = useState({} as string);
    const [toPlanet, setToPlanet] = useState({} as string);
    const [companies, setCompanies] = useState([] as ICompany[]);
    const [selectedCompanies, setSelectedCompanies] = useState([] as ICompany[]);
    const [date, setDate] = useState(new Date());
    const [planetList, setPlanetList] = useState([] as string[]);
    const [routeProviders, setRouteProviders] = useState([] as IRouteProvider[][]);
    const [fullPaths, setFullPaths] = useState([] as string[]);

    const [priceInterval, setPriceInterval] = useState(10000000000);
    const [distanceInterval, setDistanceInterval] = useState(10000000000);
    const [travelTimeInterval, setTravelTimeInterval] = useState(1000);

    let pathItemList = new Array<IPathItem[]>();


    const handleSelectedCompanies = (company: ICompany, event: ChangeEvent<HTMLInputElement>) => {

        if (selectedCompanies.length == 0) {
            return setSelectedCompanies([...selectedCompanies, company]);
        } else {
            if (event.target.checked) {
                return setSelectedCompanies([...selectedCompanies, company]);
            }
            else {
                return setSelectedCompanies(selectedCompanies.filter(comp => comp.id !== company.id));
            }
        }
    }

    const handleReservation = async (rpList: IRouteProvider[]) => {

        if (appState.token == null) {
            return navigate("/login", { replace: true });
        }

        let allRoutes = "";
        let totalPrice = 0;
        let totalTravelTime = 0;
        let companyNames = "";

        rpList.map(rp => {
            totalPrice += rp.provider?.price!;
            totalTravelTime += new Date(rp.provider?.flightEnd!).valueOf() - new Date(rp.provider?.flightStart!).valueOf();
            if (allRoutes == '') {
                allRoutes += rp.route?.from?.name! + ' - ' + rp.route?.to?.name!;
            } else {
                allRoutes += ' -> ' + rp.route?.from?.name! + ' - ' + rp.route?.to?.name!;
            }
            if (companyNames == '') {
                companyNames += rp.provider?.company?.name!;
            } else {
                companyNames += ', ' + rp.provider?.company?.name!;
            }
        });

        let res = {
            firstName: appState.firstName,
            lastName: appState.lastName,
            routes: allRoutes,
            totalQuotedPrice: totalPrice,
            transportationCompanyNames: companyNames,
            quotedTravelTime: totalTravelTime
        } as IReservationAdd;

        let response = await BaseService.create<IReservationAdd>('Reservations', res, appState.token ?? '');

        if (response.data && response.ok) {
            navigate("/reservations", { replace: true });
        }
    }

    const handleFromPlanet = (planet: ChangeEvent<HTMLSelectElement>) => {
        legs.forEach(leg => {
            if (leg.routeInfo?.from!.name == planet.target.value) {
                setFromPlanet(leg.routeInfo.from.name);
            }
        })
    }

    const handleToPlanet = (planet: ChangeEvent<HTMLSelectElement>) => {
        legs.forEach(leg => {
            if (leg.routeInfo?.to!.name == planet.target.value) {
                setToPlanet(leg.routeInfo.to.name);
            }
        })
    }

    function getCompanies(legList: ILeg[]) {
        let companies: Array<ICompany> = new Array();
        let companyNames: Array<string> = new Array();

        legList.map(leg => {
            if (leg.providers != null) {
                leg.providers!.map(provider => {
                    if (!companyNames.includes(provider.company?.id!)) {
                        companyNames.push(provider.company!.id);
                        companies.push(provider.company!);
                    }
                }
                );
            }

        });
        setCompanies(companies);
        setSelectedCompanies(companies);
    };

    function getPlanets(legList: ILeg[]) {
        let planetList = new Array<string>();

        legList.map(leg => {
            if (!planetList.includes(leg.routeInfo?.from!.name!)) {
                planetList.push(leg.routeInfo?.from!.name!);
            } else if (!planetList.includes(leg.routeInfo?.to!.name!)) {
                planetList.push(leg.routeInfo?.to!.name!);
            }

        })
        setPlanetList(planetList);
    }

    function initAdjList() {

        let planetList = new Array<string>();

        legs?.map(leg => {
            if (!planetList.includes(leg.routeInfo?.from!.name!)) {
                planetList.push(leg.routeInfo?.from!.name!);
            } else if (!planetList.includes(leg.routeInfo?.to!.name!)) {
                planetList.push(leg.routeInfo?.to!.name!);
            }
        })

        pathItemList = new Array<IPathItem[]>(8);

        for (let i = 0; i < 8; i++) {
            pathItemList[i] = [] as IPathItem[];
        }

        for (let i = 0; i < legs.length; i++) {
            addPlanetEdge({ id: planetList.indexOf(legs[i].routeInfo?.from?.name!), planet: legs[i].routeInfo?.from?.name } as IPathItem,
                { id: planetList.indexOf(legs[i].routeInfo?.to?.name!), planet: legs[i].routeInfo?.to?.name } as IPathItem);
        }

    }

    function addPlanetEdge(u: IPathItem, v: IPathItem) {
        pathItemList[u.id].push(v);
    }

    async function getPlanetPaths(s: string, d: string, date: Date) {

        setFullPaths([]);
        setRouteProviders([]);

        let from = {} as IPathItem;
        let to = {} as IPathItem;
        pathItemList.map(pln => {
            pln.map(pl2 => {
                if (pl2.planet == s) {
                    from = pl2;
                };
                if (pl2.planet == d) {
                    to = pl2;
                }
            })
        })

        let isVisited = new Array<boolean>(planetList.length);

        for (let i = 0; i < planetList.length; i++) {
            isVisited[i] = false;
        }

        let pathList = new Array<IPathItem>();

        pathList.push(from);

        printAllPlanetsUtil(from, to, isVisited, pathList, date);

    }

    function printAllPlanetsUtil(u: IPathItem, d: IPathItem, isVisited: Array<boolean>, localPathList: IPathItem[], date: Date) {

        if (u == (d)) {
            let fullPath = '';
            localPathList.map(path => {
                fullPath += fullPath ?? '' ? ' - ' + path.planet : path.planet;
            });

            if (!fullPaths.includes(fullPath)) {
                setFullPaths(path => [...path, fullPath]);
            }

            getPlanetPairsFromPath(localPathList, date);
            return;
        }

        isVisited[u.id] = true;

        for (let i = 0; i < pathItemList[u.id].length; i++) {
            if (!isVisited[pathItemList[u.id][i].id]) {
                localPathList.push(pathItemList[u.id][i]);
                printAllPlanetsUtil(pathItemList[u.id][i], d, isVisited, localPathList, date);
                localPathList.splice(localPathList.indexOf(pathItemList[u.id][i], 1));
            }
        }
        isVisited[u.id] = false;
    }

    async function getAllPathItems(allPaths: string[][], date: Date) {
        let res = [] as IRouteProvider[][];
        let sortedRes = [] as IRouteProvider[][];

        root.legs?.map(leg => {
            allPaths.map(path => {
                let tempList = Array<IRouteProvider>();
                let prevDateEnd = new Date();

                if (leg.routeInfo?.from?.name == path[0] && leg.routeInfo.to?.name == path[1] && leg.providers?.map(provider => {
                    if (tempList.length == 0) {
                        let routeProvider = { route: leg.routeInfo, provider: provider } as IRouteProvider;
                        tempList.push(routeProvider)
                    }
                    else if (tempList.length != 0) {
                        let routeProvider = { route: leg.routeInfo, provider: provider } as IRouteProvider;
                        prevDateEnd = provider.flightEnd;
                        tempList.push(routeProvider);
                    }
                })) {
                    res.push(tempList)
                }
            })

        })

        allPaths.map(path => {
            res.map(rpList => {
                if (path[0] == rpList[0].route?.from?.name) {
                    sortedRes.push(rpList);
                }
            })
        })

        getPaths(sortedRes);

    }

    function getPaths(sortedRoutes: IRouteProvider[][]) {

        let result = [] as IRouteProvider[][];
        let currentDate = new Date();
        let prevDate = new Date();
        function helper(arr: IRouteProvider[], i: number) {


            if (sortedRoutes[i] != undefined) {

                for (let j = 0; j < sortedRoutes[i].length; j++) {
                    currentDate = new Date(sortedRoutes[i][j].provider?.flightStart!);
                    let a = arr.slice(0);
                    if (a.length == 0 && currentDate.toLocaleDateString('ee-EE') == date.toLocaleDateString('ee-EE')) {
                        a.push(sortedRoutes[i][j])
                        prevDate = new Date(sortedRoutes[i][j].provider?.flightEnd!);
                    }
                    else if (a.length != 0 && prevDate.valueOf() < currentDate.valueOf()) {
                        a.push(sortedRoutes[i][j]);
                        prevDate = new Date(sortedRoutes[i][j].provider?.flightEnd!);
                    }

                    if (i == sortedRoutes.length - 1 && sortedRoutes.length == 1) {
                        result.push(a);
                    }
                    else if (i == sortedRoutes.length - 1) {
                        result.push(a);
                    }
                    else {
                        prevDate = new Date(sortedRoutes[i][j].provider?.flightEnd!.toString()!);
                        helper(a, i + 1);
                    }
                }

            } else {

            }
        }
        helper([], 0);

        setRouteProviders(result.filter(route => route.length == sortedRoutes.length));

    }

    function getPlanetPairsFromPath(localPathList: IPathItem[], date: Date) {

        let allPaths = [] as string[][];

        for (let i = 0; i < localPathList.length; i++) {
            let planetPair = new Array<string>();
            if (i != localPathList.length - 1) {
                planetPair.push(localPathList[i].planet);
                planetPair.push(localPathList[i + 1].planet)
                allPaths.push(planetPair);
            }
        }

        getAllPathItems(allPaths, date);

    }

    const loadData = async () => {

        let res = await CosmosService.getCosmosInfo<IRoot>();

        if (res.ok && res.data) {

            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });

            setRoot(res.data);

            setLegs(res.data.legs!);

            getPlanets(res.data.legs!);

            getCompanies(res.data.legs!);

            setDate(new Date());

        } else {

            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: res.statusCode });

        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            {/* Tean, et seda ei tohiks siin teha, kuid see on kiire lahendus mis tuleb hiljem ära parandada kindlasti. */}
            {initAdjList()}
            <div className="d-flex flex-row mb-3">
                <div className="text-dark bg-light">
                    <label />
                    {companies!.map(company =>
                        <>
                            <label className="ms-3 mt-3 me-1" htmlFor="checkCompany">{company.name}</label>
                            <input className="me-3" type="checkbox" key={company.id} id="checkCompany" defaultChecked={true} onChange={e => handleSelectedCompanies(company, e)}></input>
                            <br />
                        </>
                    )}
                    <br />

                    <label>Price Interval (€)</label>
                    <RangeSlider
                        value={priceInterval}
                        min={0}
                        max={1000000000}
                        step={10000}
                        onChange={changeEvent => setPriceInterval(changeEvent.target.valueAsNumber)}
                    />

                    <label>Distance Interval (km)</label>
                    <RangeSlider
                        value={distanceInterval}
                        min={0}
                        max={10000000000}
                        step={1000000}
                        onChange={changeEvent => setDistanceInterval(changeEvent.target.valueAsNumber)}
                    />

                    <label>Travel Time Interval (h)</label>
                    <RangeSlider
                        value={travelTimeInterval}
                        min={0}
                        max={1000}
                        step={1}
                        onChange={changeEvent => setTravelTimeInterval(changeEvent.target.valueAsNumber)}
                    />
                </div>

                <div className="content">
                    <div className="d-flex flex-row mb-3">
                        <input className="form-control me-3 ms-3" type="date" defaultValue={new Date().toISOString().substr(0, 10)}
                            onChange={date => setDate(new Date(date.target.value))}></input>
                        <select className="form-control me-3" id="fromPlanet" name="fromPlanet.id" value={fromPlanet}
                            onChange={planet => handleFromPlanet(planet)}>
                            <option value="">--- Select FROM ---</option>
                            {planetList.map(value => <option value={value} key={value} >{value}</option>)}
                        </select>
                        <select className="form-control me-3" id="toPlanet" name="toPlanet.id" value={toPlanet}
                            onChange={planet => handleToPlanet(planet)}>
                            <option value="">--- Select TO ---</option>
                            {planetList.map(value => <option value={value} key={value} >{value}</option>)}
                        </select>
                        <button className="btn btn-primary" onClick={() => { getPlanetPaths(fromPlanet, toPlanet, date) }}>SEARCH</button>
                    </div>

                    <table>
                        <tbody>
                            {pageStatus.statusCode == 403 ?
                                <div>
                                    <p>Request temporary access to demo server on the following link: </p>
                                    <button className='btn btn-secondary' onClick={() => { window.open("https://cors-anywhere.herokuapp.com/corsdemo") }}>Request Temporary Access</button>
                                </div>
                                :
                                <></>
                            }
                            {routeProviders.length == 0 ?
                                <div>
                                    <tr>
                                        <td>
                                            {fullPaths.map(path =>
                                                <>
                                                    <tr>
                                                        <td>
                                                            {path}
                                                        </td>
                                                    </tr>
                                                </>
                                            )}
                                            <p>No Paths Found</p>
                                        </td>
                                    </tr>
                                </div> : fullPaths.map(path =>
                                    <>
                                        <tr>
                                            <td>
                                                {path}
                                            </td>
                                        </tr>
                                    </>
                                )}

                            {routeProviders.map(rpList =>
                                <tr>
                                    <td>
                                        <div className="text-dark bg-light ms-3">
                                            {rpList.filter(filtered => selectedCompanies.some(element => filtered.provider?.company?.id == element.id))
                                                .filter(filtered => filtered.provider?.price! < priceInterval)
                                                .filter(filtered => filtered.route?.distance! < distanceInterval)
                                                .filter(filtered => ((new Date(filtered.provider?.flightEnd!).valueOf()! - new Date(filtered.provider?.flightStart!).valueOf()!) / (1000 * 60 * 60) < travelTimeInterval))
                                                .map(rp =>
                                                    <>
                                                        <div className="d-flex flex-row">
                                                            {() => {
                                                                if (selectedCompanies.length != 0) {

                                                                }
                                                            }}

                                                            <div className="d-flex flex-column mb-3">
                                                                <div className="d-flex flex-row mb-3">

                                                                    <div className="p-2">
                                                                        {"START: "}
                                                                        {new Date(rp.provider!.flightStart.toString()).toLocaleString('ee-EE')}
                                                                    </div>
                                                                    <div className="p-2">
                                                                        {"END: "}
                                                                        {new Date(rp.provider!.flightEnd.toString()).toLocaleString('ee-EE')}
                                                                    </div>
                                                                    <div className="p-2">
                                                                        {"FLIGHT TIME: "}
                                                                        {msToTime(new Date(rp.provider!.flightStart.toString()).valueOf() - new Date(rp.provider!.flightEnd.toString()).valueOf())}
                                                                    </div>
                                                                </div>
                                                                <div className="d-flex flex-row mb-3">
                                                                    <div className="p-2">
                                                                        {"FROM: "}
                                                                        {rp.route!.from?.name}
                                                                    </div>
                                                                    <div className="p-2">
                                                                        {"TO: "}
                                                                        {rp.route?.to?.name}
                                                                    </div>
                                                                    <div className="p-2">
                                                                        {"DISTANCE: "}
                                                                        {rp.route!.distance}
                                                                    </div>
                                                                    <div className="p-2">
                                                                        {"COMPANY: "}
                                                                        {rp.provider!.company?.name}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="d-flex flex-column mb-3">
                                                                <div className="p-2">
                                                                    {"PRICE: "}
                                                                    {rp.provider!.price}<br />
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </>
                                                )
                                            }
                                            <div className="p-2 mb-3">
                                                <button className="btn btn-secondary" onClick={() => { handleReservation(rpList) }}>RESERVE</button>
                                            </div>
                                            <span />
                                        </div>
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
            <Loader {...pageStatus} />
        </>
    );
}


export default RootIndex;