import { IProvider } from "./IProvider";
import { IRouteInfo } from "./IRouteInfo";

export interface ILeg {
    id: string,
    routeInfo: IRouteInfo | null,
    providers: IProvider[] | null
}