import { ICompany } from "./ICompany";

export interface IProvider {
    id: string,
    company: ICompany | null,
    price: number,
    flightStart: Date,
    flightEnd: Date
}