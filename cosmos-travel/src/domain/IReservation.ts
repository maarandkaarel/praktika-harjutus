export interface IReservation {
    id: string,
    firstName: string,
    lastName: string,
    routes: string,
    totalQuotedPrice: number,
    quotedTravelTime: number,
    transportationCompanyNames: string
    // validUntil: Date
}

export interface IReservationAdd {
    firstName: string,
    lastName: string,
    routes: string,
    totalQuotedPrice: number,
    quotedTravelTime: number,
    transportationCompanyNames: string
    // validUntil: Date
}