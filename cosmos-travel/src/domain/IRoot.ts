import { ILeg } from "./ILeg";

export interface IRoot {
    id: string,
    validUntil: Date,
    legs: ILeg[] | null
}