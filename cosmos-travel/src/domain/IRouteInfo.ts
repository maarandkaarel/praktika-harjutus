import { IPlanet } from "./IPlanet";

export interface IRouteInfo {
    id: string,
    from: IPlanet | null,
    to: IPlanet | null,
    distance: number
}