import { IProvider } from "./IProvider";
import { IRouteInfo } from "./IRouteInfo";

export interface IRouteProvider {
    route: IRouteInfo | null,
    provider: IProvider | null
}