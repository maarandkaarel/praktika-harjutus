import React from "react";
import { useContext, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import Alert, { EAlertClass } from "../components/Alert";
import { AppContext } from "../context/AppState";
import { IdentityService } from "../services/identity-service";

import jwt_decode, { JwtPayload } from "jwt-decode";
import { IJwtLoad } from '../types/IJwtLoad';

const Login = () => {
    const appState = useContext(AppContext);

    const [loginData, setLoginData] = useState({ email: '', password: '' });
    const [alertMessage, setAlertMessage] = useState('');

    let navigate = useNavigate();

    const logInClicked = async (e: Event) => {

        e.preventDefault();
        if (loginData.email === '' || loginData.password === '') {
            setAlertMessage('Empty email or password!');
        };
        setAlertMessage('');
        let response = await IdentityService.Login('account/login', loginData);
        if (!response.ok) {
            setAlertMessage(response.messages![0]);
        } else {
            var jwt = jwt_decode(response.data?.token!) as JwtPayload extends IJwtLoad ? IJwtLoad : IJwtLoad;
            const roles = jwt["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
            console.log(roles)
            console.log(response.data?.token);
            console.log(response.data?.firstName);
            console.log(response.data?.lastName);
            setAlertMessage('');
            appState.setAuthInfo(response.data!.token, response.data!.firstName, response.data!.lastName, roles!);
            navigate("/", { replace: true });
        }
    }

    return (
        <>
            {appState.token !== null ? <Navigate replace to="/" /> : null}
            <h1>Log in</h1>
            <form onSubmit={(e) => logInClicked(e.nativeEvent)}>
                <div className="row">
                    <div className="col-md-6">
                        <section>
                            <hr />
                            <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />
                            <div className="form-group">
                                <label htmlFor="Input_Email">Email</label>
                                <input value={loginData.email} onChange={e => setLoginData({ ...loginData, email: e.target.value })} className="form-control" type="email" id="Input_Email" name="Input.Email" placeholder="user@example.com" autoComplete="username" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="Input_Password">Password</label>
                                <input value={loginData.password} onChange={e => setLoginData({ ...loginData, password: e.target.value })} className="form-control" type="password" id="Input_Password" name="Input.Password" placeholder="Input your current password..." autoComplete="current-password" />
                            </div>
                            <div className="form-group">
                                <button onClick={(e) => logInClicked(e.nativeEvent)} type="submit" className="btn btn-primary">Log in</button>
                            </div>
                        </section>
                    </div>
                </div>
            </form>
        </>
    )
}

export default Login;