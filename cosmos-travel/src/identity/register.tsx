import React from "react";
import { useContext, useState } from "react";
import { Navigate } from "react-router-dom";
import Alert, { EAlertClass } from "../components/Alert";
import { AppContext } from "../context/AppState";
import { IdentityService } from "../services/identity-service";

import jwt_decode, { JwtPayload } from "jwt-decode";
import { IJwtLoad } from '../types/IJwtLoad';

const Register = () => {
    const appState = useContext(AppContext);

    const [RegisterData, setRegisterData] = useState({ email: '', password: '', firstName: '', lastName: '' });
    const [alertMessage, setAlertMessage] = useState('');

    const RegisterClicked = async (e: Event) => {
        e.preventDefault();
        if (RegisterData.email === '' || RegisterData.password === '' || RegisterData.firstName === '' || RegisterData.lastName === '') {
            setAlertMessage('Empty fields!');
        };
        setAlertMessage('');
        let response = await IdentityService.Register('account/Register', RegisterData);
        if (!response.ok) {
            setAlertMessage(response.messages![0]);
        } else {
            var jwt = jwt_decode(response.data?.token!) as JwtPayload extends IJwtLoad?IJwtLoad:IJwtLoad;
            console.log("token: " + response.data?.token);
            console.log("jwt: " + jwt);
            const roles = jwt["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
            console.log(roles)
            setAlertMessage('');
            appState.setAuthInfo(response.data!.token, response.data!.firstName, response.data!.lastName, roles!);
        }
    }

    return (
        <>
            { appState.token !== null ? <Navigate replace to="/" /> : null}
            <h1>Register</h1>
            <form onSubmit={(e) => RegisterClicked(e.nativeEvent)}>
                <div className="row">
                    <div className="col-md-6">
                        <section>
                            <hr />
                            <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />
                            <div className="form-group">
                                <label htmlFor="Input_Email">Email</label>
                                <input value={RegisterData.email} onChange={e => setRegisterData({ ...RegisterData, email: e.target.value })} className="form-control" type="email" id="Input_Email" name="Input.Email" placeholder="user@example.com" autoComplete="username" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="Input_Password">Password</label>
                                <input value={RegisterData.password} onChange={e => setRegisterData({ ...RegisterData, password: e.target.value })} className="form-control" type="password" id="Input_Password" name="Input.Password" placeholder="Input your desired password..." autoComplete="current-password" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="Input_ConfirmPassword">Confirm Password</label>
                                <input /* password confirmation should be done */ className="form-control" type="password" id="Input_ConfirmPassword" name="Input.ConfirmPassword" placeholder="Confirm the password..." autoComplete="current-password" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="Input_FirstName">First Name</label>
                                <input value={RegisterData.firstName} onChange={e => setRegisterData({ ...RegisterData, firstName: e.target.value })} className="form-control" type="text" id="Input_FirstName" name="Input.FirstName" placeholder="Input your firstname..." autoComplete="firstName" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="Input_LastName">Last Name</label>
                                <input value={RegisterData.lastName} onChange={e => setRegisterData({ ...RegisterData, lastName: e.target.value })} className="form-control" type="text" id="Input_LastName" name="Input.LastName" placeholder="Input your lastname..." autoComplete="lastName" />
                            </div>
                            <div className="form-group">
                                <button onClick={(e) => RegisterClicked(e.nativeEvent)} type="submit" className="btn btn-primary">Register</button>
                            </div>
                        </section>
                    </div>
                </div>
            </form>
        </>
    )
}

export default Register;