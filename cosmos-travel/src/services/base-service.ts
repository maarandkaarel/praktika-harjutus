import Axios, { AxiosError, AxiosRequestConfig } from "axios";
import { useEffect } from "react";
import { ApiBaseUrl, ApiRouteBaseUrl } from "../configuration";
import { IFetchResponse } from "../types/IFetchResponse";
import { IMessages } from "../types/IMessages";

export abstract class BaseService {
    protected static axios = Axios.create({
        baseURL: ApiBaseUrl,
        headers: {
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*"
        },
        withCredentials: false
    });

    protected static getAxiosConfiguration(token?: string): AxiosRequestConfig | undefined {
        if (!token) return undefined;
        const config: AxiosRequestConfig = {
            headers: {
                Authorization: 'Bearer ' + token
            }
        };
        return config;
    }

    static async getAll<TEntity>(apiEndpoint: string, token?: string): Promise<IFetchResponse<TEntity[]>> {
        try {
            let response = await this.axios.get<TEntity[]>(apiEndpoint, BaseService.getAxiosConfiguration(token));
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500,
                messages: (err.response?.data as IMessages).messages
            };
        }
    }

    static async get<TEntity>(apiEndpoint: string, id: string, token?: string): Promise<IFetchResponse<TEntity>> {
        let url = ApiBaseUrl + apiEndpoint + id;

        try {
            let response = await this.axios.get<TEntity>(url, BaseService.getAxiosConfiguration(token));
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500,
                messages: (err.response?.data as IMessages).messages
            };
        }
    }

    static async create<TEntityAdd>(apiEndpoint: string, entity: TEntityAdd, token?: string) {
        try {
            const response = await this.axios.post(ApiBaseUrl + apiEndpoint, JSON.stringify(entity), BaseService.getAxiosConfiguration(token));
            console.log(response.status <= 299);
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500,
                data: (err.response?.data as IMessages).messages
            }
        }
    }

    static async edit<Tentity>(apiEndpoint: string, id: string, entity: Tentity, token?: string) {
        const url = ApiBaseUrl + apiEndpoint + id;
        try {
            const response = await this.axios.put(url, JSON.stringify(entity), BaseService.getAxiosConfiguration(token));
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500,
                messages: (err.response?.data as IMessages).messages
            };
        }
    }

    static async delete(apiEndpoint: string, id: string, token?: string){
        const url = ApiBaseUrl + apiEndpoint + id;
        try {
            const response = await this.axios.delete(url, BaseService.getAxiosConfiguration(token));
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500,
                messages: (err.response?.data as IMessages).messages
            };
        }
    }
}