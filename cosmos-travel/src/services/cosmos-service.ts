import Axios, { AxiosError, AxiosRequestConfig } from "axios";
import { useEffect } from "react";
import { ApiRouteBaseUrl } from "../configuration";
import { IFetchResponse } from "../types/IFetchResponse";
import { IMessages } from "../types/IMessages";

export abstract class CosmosService {
    
    protected static axios = Axios.create({
        baseURL: ApiRouteBaseUrl,
        headers: {
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*"
        },
        withCredentials: false
    });

    static async getCosmosInfo<TEntity>(): Promise<IFetchResponse<TEntity>>{
        let url = ApiRouteBaseUrl;
        try {
            let response = await this.axios.get<TEntity>(url);
            console.log(response.status);
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500,
                messages: (err.response?.data as IMessages).messages
            }
        }
    }
}