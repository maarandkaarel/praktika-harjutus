import Axios, { AxiosError } from "axios";
import { ApiBaseUrl } from "../configuration";
import { IFetchResponse } from "../types/IFetchResponse";
import { ILoginResponse } from "../types/ILoginResponse"
import { IMessages } from "../types/IMessages";
import { IRegisterResponse } from "../types/IRegisterResponse";

export abstract class IdentityService {
    protected static axios = Axios.create({
        baseURL: ApiBaseUrl,
        headers: {
            'Content-Type': 'application/json'
        }
    });

    static async Login(apiEndPoint: string, loginData: {email: string, password: string}): Promise<IFetchResponse<ILoginResponse>> {
        let loginDataJson = JSON.stringify(loginData);
        try {
            let response = await this.axios.post<ILoginResponse>(apiEndPoint, loginDataJson);
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            };
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500,
                messages: (err.response?.data as IMessages).messages,
            }
        }
    }

    static async Register(apiEndPoint: string, registerData: {email: string, password: string, firstName: string, lastName: string}): Promise<IFetchResponse<IRegisterResponse>> {
        let registerDataJson = JSON.stringify(registerData);
        try {
            let response = await this.axios.post<IRegisterResponse>(apiEndPoint, registerDataJson);
            console.log(response.data.token);
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            };
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500,
                messages: (err.response?.data as IMessages).messages,
            }
        }
      }
}