export interface IRegisterResponse {
    token: string;
    firstName: string;
    lastName: string;
}